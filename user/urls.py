from . import views
from django.urls import path


urlpatterns = [
    path('login/',views.loginPage,name='login'),
    path('register/', views.registerPage,name='register'),
    path('home/', views.homePage,name='home'),
    path('',views.logoutUser,name='logout')
]