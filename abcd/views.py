
from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def index(request):
    return render(request,'abcd/home.html')
def re_choose_option(request):
    return  render(request,'abcd/choose_option.html')
def boardgame3vs3(request):
    return  render(request,'abcd/boardgame3vs3.html')
def boardgame5vs5(request):
    return  render(request,'abcd/boardgame5vs5.html')
def boardgame10vs10(request):
    return  render(request,'abcd/boardgame10vs10.html')

def re_choose_option_ver2(request):
    return  render(request,'abcd/choose_option_ver2.html')
def board_com3vs3(request):
    return  render(request,'abcd/board_com3vs3.html')
def board_com5vs5(request):
    return  render(request,'abcd/board_com5vs5.html')
def board_com10vs10(request):
    return  render(request,'abcd/board_com10vs10.html')