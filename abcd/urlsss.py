from django.urls import path
from . import views
urlpatterns = [
    path('home/',views.index),
    path('choose_option/',views.re_choose_option,name='choose_option'),
    path('choose_option_ver2/',views.re_choose_option_ver2,name='choose_option_ver2'),
    path('boardgame3vs3/',views.boardgame3vs3,name='boardgame3vs3'),
    path('boardgame5vs5/',views.boardgame5vs5,name='boardgame5vs5'),
    path('boardgame10vs10/',views.boardgame10vs10,name='boardgame10vs10'),
    path('board_com3vs3/',views.board_com3vs3,name='board_com3vs3'),
    path('board_com5vs5/',views.board_com5vs5,name='board_com5vs5'),
    path('board_com10vs10/',views.board_com10vs10,name='board_com10vs10'),
]
