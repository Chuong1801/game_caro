function WinGame() {
  //WinHor
  let Board = GetBoard();
  let result = null;
  for (let i = 0; i < 3; i++) {
	if (Board[i + 0 * size] === Board[i + 1 * size] === Board[i +2 * size])
		result = -10;
	else result = 10;
  }
  //WinVer
  for (let i = 0; i < 3; i++) {
	if (Board[0 + i * size] === Board[1 + i * size] === Board[2 + i * size])
		result = -10;
	else result = 10;
  }
  //WinCross1
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
		if (Board[i + j * size] === player)
	 		result = -10;
		else result = 10;
    }
  }
  //WinCross2
  for (let i = 2; i >= 0; i++) {
    for (let j = 0; j < 3; j++) {
	  if (Board[i + j * size] === player)
	  	result = -10;
	  else result = 10;
    }
  }
  let openSpots = 0;
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (Board[i + j * size] === -1) {
        openSpots++;
      }
    }
  }
  if (result == null && openSpots == 0) {
    return 0;
  } else {
    return result;
  }
}