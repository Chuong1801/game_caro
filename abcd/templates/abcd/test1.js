const size = 3;
const countmax = 3;
let CPlayer = 0; // Current Player (0 is O,1 is X)
let InGame = false;
let l_played = [];
let l_win = [];
let mode = 0;
let AI = false;
// let score = {
//     0: -10,
//     1: 10,
//     tie: 0
// }
let score = [-10, 10];

function AIcheck() {
    AI = true
}

function Loaded() {
    mode = 1;
    InGame = true;
    CPlayer = 0; // Current Player (0 is O,1 is X)
    l_played = [], l_win = [];
    //	let imgp = document.getElementById("imgPlayer");
    //	imgp.style.backgroundImage = "url('player_2.png')";
    let table = document.getElementById("table");
    let row = document.getElementsByClassName("row");
    let square = document.getElementsByClassName("square");
    // Create Table
    table.innerHTML = "";
    for (y = 0; y < size; y++) {
        table.innerHTML += '<tr class="row"></tr>';
        for (x = 0; x < size; x++) {
            let div = '<div class="square" onClick="Click(id)" onMouseOver="MouseOver(id)" onMouseOut="MouseOut(id)"></div>';
            row.item(y).innerHTML += '<td class="col">' + div + '</td>';
            square.item(x + y * size).setAttribute("id", (x + y * size).toString());
            square.item(x + y * size).setAttribute("player", "-1");
        }
    }
}

function Click(id) {
    if (!InGame)
        return;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    if (square.item(pos).getAttribute("player") != "-1")
        return;
    let path = "url('../../../static/image/player_2.png')";
    if (CPlayer == 1) {
        path = "url('../../../static/image/player_1.png')";
    }
    square.item(pos).style.backgroundImage = path;
    square.item(pos).setAttribute("player", CPlayer.toString());
    l_played.push(pos);
    let win = WinGame();
    let pwin = CPlayer;
    if (!AI) {
        if (CPlayer == 0) {
            CPlayer = 1;
        } else {
            CPlayer = 0;
        }
    } else {
        if (!win) {
            AIMode();
            win = WinGame();
            pwin = 1;
        }
    }
    if (win) {
        let mess = 'Player with "X" win';
        if (pwin == 0)
            mess = 'Player with "O" win';
        alert(mess);
        InGame = false;
        let square = document.getElementsByClassName("square");
        for (x = 0; x < size; x++) {
            for (y = 0; y < size; y++) {
                let pos = parseInt(x+y*size);
                square.item(pos).style.backgroundColor = "#FFF";
            }
        }
    } else if (l_played.length == size * size && !win) {
        alert("Draw!!");
        InGame = false;
        let square = document.getElementsByClassName("square");
        for (x = 0; x < size; x++) {
            for (y = 0; y < size; y++) {
                let pos = parseInt(x+y*size);
                square.item(pos).style.backgroundColor = "#FFF";
            }
        }
    }
}

function MouseOver(id) {
    if (!InGame) return;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    square.item(pos).style.backgroundColor = "#3F3";
}

function MouseOut(id) {
    if (!InGame) return;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    square.item(pos).style.backgroundColor = "#FFF";
}

function minab(a, b) {
    if (a < b) return a;
    else return b;
}

function maxab(a, b) {
    if (a > b) return a;
    else return b;
}

function WinGame() {
    let result = false;
    let Board = GetBoard();
    for (x = 0; x < size; x++) {
        for (y = 0; y < size; y++) {
            if (winHor(x, y, Board) || winVer(x, y, Board) || winCross1(x, y, Board) || winCross2(x, y, Board)) {
                let square = document.getElementsByClassName("square");
                for (i = 0; i < l_win.length; i++) {
                    square.item(l_win[i]).style.backgroundColor = "#F3EC20";
                }
                 result = true;
                //                result = Board[x + y * size];
            }
        }
    }
    return result;
}

function winHor(x, y, Board) {
    l_win = [];
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1)
        return false;
    if (x > 0) {
        let p = Board[x - 1 + y * size];
        if (p != player && p != -1)
            counto++;
    }
    for (i = x; i < size; i++) {
        let p = Board[i + y * size];
        if (p == player && p != -1) {
            count++;
            l_win.push(i + y * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winVer(x, y, Board) {
    l_win = [];
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1) return false;
    if (y > 0) {
        let p = Board[x + (y - 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = y; i < size; i++) {
        let p = Board[x + i * size];
        if (p == player && p != -1) {
            count++;
            l_win.push(x + i * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winCross1(x, y, Board) {
    l_win = [];
    if (x > size - countmax || y < countmax - 1) return false;
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1)
        return false;
    if (y < size - 1 && x > 0) {
        let p = Board[x - 1 + (y + 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = 0; i <= minab(size - x, y); i++) {
        let p = Board[(x + i) + (y - i) * size];
        if (p == player && p != -1) {
            count++;
            l_win.push((x + i) + (y - i) * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winCross2(x, y, Board) {
    l_win = [];
    if (x > size - countmax || y > size - countmax) return false;
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1) return false;
    if (y > 0 && x > 0) {
        let p = Board[x - 1 + (y - 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = 0; i < minab(size - x, size - y); i++) {
        let p = Board[(x + i) + (y + i) * size];
        if (p == player && p != -1) {
            count++;
            l_win.push((x + i) + (y + i) * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function GetBoard() {
    let TBoard = [];
    let sqr = document.getElementsByClassName("square");
    for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
        TBoard.push(parseInt(sqr.item(x+y*size).getAttribute("player")));}}
    return TBoard;
}

function Reset() {
    AI = false;
    Loaded();
    InGame = true;
}

function PvsM() {
    AI = true;
    Loaded();
    InGame = true;
}

function AIMode() {
    if (!InGame) return;
    let vmax = -99;
    let px = -1;
    let py= -1;
    let TBoard = GetBoard();
    for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
            if (TBoard[x + y * size] == -1) {
                TBoard[x + y * size] = 1;
                let mark = minimax(TBoard, 0, false);
                TBoard[x + y * size] = -1;
                if (mark > vmax) {
                    px = x;
                    py = y;
                    vmax = mark;
                }
            }
        }
    }
    try {
        let sqr = document.getElementsByClassName("square");
        sqr.item(px + py * size).setAttribute("player", "1");
        sqr.item(px + py * size).style.backgroundImage = "url('../../../static/image/player_1.png')";
        l_played.push(px + py * size);
    } catch (e) {}
}

function GetMark(TBoard) {
    let result = null;
    let openSpots = 0;
    for (let x = 0; x < size; x++) {
        for (let y = 0; y < size; y++) {
            if (winHor(x, y, TBoard) || winVer(x, y, TBoard) || winCross1(x, y, TBoard) || winCross2(x, y, TBoard)) {
                let player = TBoard[x + y * size];
                if(player==1)
                    result = 1;
                 else if(player==0)
                    result = 0;
            }
            if (TBoard[x+y*size] == -1) {
                openSpots++;
            }

       }
    }

    if (result == null && openSpots == 0) {
        return -1;
    } else
        return result;
}

function minimax(TBoard, depth, isMaximizing) {
    let result = GetMark(TBoard);
    if (result !== null) {
        if (result == 1)
            return score[1];
        else if (result == 0)
            return score[0];
        else return 0;
    }
    if (isMaximizing==true) {
    let bestScore = -99;
        for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
        if (TBoard[x + y * size] == -1) {
            TBoard[x + y * size] = 1;
            let score = minimax(TBoard, depth + 1, false);
            TBoard[x + y * size] = -1;
            bestScore = maxab(score, bestScore);
        }}
        }
        return bestScore;
    } else {
    let bestScore = 99;
        for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
        if (TBoard[x + y * size] == -1) {
            TBoard[x + y * size] = 0;
            let score = minimax(TBoard, depth + 1, false);
            TBoard[x + y * size] = -1;
            bestScore = minab(score, bestScore);
        }}
        }
        return bestScore;
    }
}