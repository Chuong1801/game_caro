const size = 3;
    const countmax = 3;
    var CPlayer = 0; // Current Player (0 is O,1 is X)
    var InGame = false;
    var l_played = [];
    var l_win = [];
    var mode = 0;
    var human="url('../../static/image/player_2.png')";
    var aio="url('../../static/image/player_1.png')";
    var AI = false;
function AIcheck()
{
    AI=true
}
//New Game
function Loaded()
{
    mode=1;
    InGame = true;
	CPlayer = 0; // Current Player (0 is O,1 is X)
	l_played = [], l_win = [];
//	var imgp = document.getElementById("imgPlayer");
//	imgp.style.backgroundImage = "url('player_2.png')";

	var table = document.getElementById("table");
	var	row = document.getElementsByClassName("row");
	var square = document.getElementsByClassName("square");

	// Create Table
	table.innerHTML = "";
	for (y = 0; y < size; y++)
	{
		table.innerHTML += '<tr class="row"></tr>';
		for (x = 0;x < size; x++)
		{
			var div = '<div class="square" onClick="Click(id)" onMouseOver="MouseOver(id)" onMouseOut="MouseOut(id)"></div>';
			row.item(y).innerHTML += '<td class="col">' + div + '</td>';
			square.item(x+y*size).setAttribute("id",(x+y*size).toString());
			square.item(x+y*size).setAttribute("player","-1");
		}
	}


}
function Click(id)
{
	if (!InGame)
	    return;
	var square = document.getElementsByClassName("square");
	var pos = parseInt(id);
	if (square.item(pos).getAttribute("player") != "-1")
	    return;
	var path ="url( '../../static/image/player_2.png' )";
	if (CPlayer == 1)
	{
	    path = "url('../../static/image/player_1.png')";
	}
    square.item(pos).style.backgroundImage = path;
	square.item(pos).setAttribute("player",CPlayer.toString());
	l_played.push(pos);
	var TBoard=GetBoard();
	var win = WinGame();
	var pwin = CPlayer;
	if (!AI)
	{
		if (CPlayer == 0)
		{
		    CPlayer = 1;
		}
		else
		{
		    CPlayer = 0;
		}
	}
	else
	{
		if (!win)
		{
            if(CPlayer==0)
            {
                if(square.item(pos).getAttribute("player") != "-1")
                {
                    square.item(pos).setAttribute("player","0");
                    CPlayer = 1;
                    AIMode();
                    win = WinGame();
                    pwin = 1;
                }
            }

		}
	}

	if (win)
	{
		var mess = 'Player with "X" win';
        if (pwin == 0)
            mess = 'Player with "O" win';
		alert(mess);
		InGame = false;
	}
	else if(l_played.length==size*size && !win)
	    alert("Draw!!");
}
function MouseOver(id)
{
	if (!InGame) return;
	var square = document.getElementsByClassName("square");
	var pos = parseInt(id);
	square.item(pos).style.backgroundColor = "#3F3";
}

function MouseOut(id)
{
	if (!InGame) return;
	var square = document.getElementsByClassName("square");
	var pos = parseInt(id);
	square.item(pos).style.backgroundColor = "#FFF";
}
function minab(a,b)
{
	if (a < b) return a;
	else return b;
}
function WinGame()
{
	var result = false;
	var Board = GetBoard();
	for (x = 0;x < size;x++)
	{
		for (y = 0;y < size;y++)
		{
			if (winHor(x,y,Board) || winVer(x,y,Board) || winCross1(x,y,Board) || winCross2(x,y,Board))
			{
				var square = document.getElementsByClassName("square");
				for(i = 0;i < l_win.length;i++)
				{
					square.item(l_win[i]).style.backgroundColor = "#F3EC20";
				}
				result = true;
			}
		}
	}
	return result;
}

// Win Dir
function winHor(x,y,Board,player)
{
	l_win = [];
	var count = 0,counto=0;
	var player = Board[x + y*size];
	if (player == -1) return false;

	if (x > 0)
	{
		var p = Board[x-1+y*size];
		if (p != player && p != -1)
		    counto++;
	}

	for (i = x; i < size;i++)
	{
		var p = Board[i+y*size];
		if (p == player && p != -1)
		{
			count++;
			l_win.push(i+y*size);
		}
		else{ if (p != -1) counto++;break;};
	}
	if (count >= countmax)
	{
		if (mode == 0)
		return true;
		else {
				if (counto >= 2) return false;
				else return true;
			 }
	}
	return false;
}

function winVer(x,y,Board,player)
{
	l_win = [];
	var count = 0,counto=0;
	var player = Board[x + y*size];
	if (player == -1) return false;

	if (y > 0)
	{
		var p = Board[x+(y-1)*size];
		if (p != player && p != -1) counto++;
	}

	for (i = y; i < size;i++)
	{
		var p = Board[x+i*size];
		if (p == player && p != -1)
		{
			count++;
			l_win.push(x+i*size);
		}
		else{ if (p != -1) counto++;break;};
	}
	if (count >= countmax)
	{
		if (mode == 0)
		return true;
		else {
				if (counto >= 2) return false;
				else return true;
			 }
	}
	return false;
}

function winCross1(x,y,Board,player)
{
	l_win = [];
	if (x > size-countmax || y < countmax-1) return false;
	var count = 0, counto = 0;
	var player = Board[x + y*size];
	if (player == -1)
	    return false;
	if (y < size-1 && x > 0)
	{
		var p = Board[x-1+(y+1)*size];
		if (p != player && p != -1) counto++;
	}

	for (i = 0; i <= minab(size-x,y);i++)
	{
		var p = Board[(x+i)+(y-i)*size];
		if (p == player && p != -1)
		{
			count++;
			l_win.push((x+i)+(y-i)*size);
		}
		else{ if (p != -1) counto++;break;};
	}
	if (count >= countmax)
	{
		if (mode == 0)
		return true;
		else {
				if (counto >= 2) return false;
				else return true;
			 }
	}
	return false;
}

function winCross2(x,y,Board,player)
{
	l_win = [];
	if (x > size-countmax || y > size-countmax) return false;
	var count = 0, counto = 0;
	var player = Board[x + y*size];
	if (player == -1) return false;
	if (y > 0 && x > 0)
	{
		var p = Board[x-1+(y-1)*size];
		if (p != player && p != -1) counto++;
	}

	for (i = 0; i < minab(size-x,size-y);i++)
	{
		var p = Board[(x+i)+(y+i)*size];
		if (p == player && p != -1)
		{
			count++;
			l_win.push((x+i)+(y+i)*size);
		}
		else{ if (p != -1) counto++;break;};
	}
	if (count >= countmax)
	{
		if (mode == 0)
		return true;
		else {
				if (counto >= 2) return false;
				else return true;
			 }
	}
	return false;
}
function GetBoard()
{
	var TBoard = [];
	var sqr = document.getElementsByClassName("square");
	for (i = 0; i < size*size;i++)
		TBoard.push(parseInt(sqr.item(i).getAttribute("player")));

	return TBoard;
}
// Button Event
function Reset()
{
	AI = false;
	Loaded();
	InGame = true;
}
//
function PvsM()
{
	AI = true;
	Loaded();
	InGame = true;
}

function AIMode()
{
	if (!InGame) return;
	var bestScore = -Infinity;
	var move;
	var px = py = -1;
	var TBoard = GetBoard();
	for (y = 0; y < size; y++)
	{
		for (x = 0; x < size; x++)
		{
			if (TBoard[x+y*size] == -1)
			{
				TBoard[x+y*size] = 1;
				var score = minimax(TBoard, 0, false);
				TBoard[x+y*size]= -1;
				if (score > bestScore)
				{
					bestScore = score;
					px = x;py = y;
				}
			}
		}
	}
	try
	{
		var sqr = document.getElementsByClassName("square");
		sqr.item(px + py*size).setAttribute("player","1");
		sqr.item(px + py*size).style.backgroundImage = "url('../../../static/image/player_1.png')";
		l_played.push(px+py*size);
	}
	catch(e) {}
}
var scores = {
  human: -10,
  aio: 10,
  tie: 0
};

function minimax(TBoard, depth, isMaximizing) {
  var result = WinGame();
  if (result !== null) {
    if()
    return scores[result];
  }

  if (isMaximizing)
  {
    var bestScore = -Infinity;
    for (var i = 0; i < size; i++)
    {
      for (var j = 0; j < size; j++)
      {
        if (TBoard[x+y*size] == -1)
        {
          TBoard[x+y*size] = 1;
          var score = minimax(TBoard, depth + 1, false);
          TBoard[x+y*size] = -1;
          bestScore = max(score, bestScore);
        }
      }
    }
    return bestScore;
  } else
  {
    var bestScore = Infinity;
    for (var i = 0; i < size; i++)
    {
      for (var j = 0; j < size; j++)
      {
        if (TBoard[x+y*size] == '')
        {
          TBoard[x+y*size] = human;
          var score = minimax(TBoard, depth + 1, true);
          TBoard[x+y*size] = '';
          bestScore = min(score, bestScore);
        }
      }
    }
    return bestScore;
  }
 }
