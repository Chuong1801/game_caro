const size = 3;
const countmax = 3;
let CPlayer = 0; // Current Player (0 is O,1 is X)
let InGame = false;
let l_played = [];
let l_win = [];
let mode = 0;
let MAXDEPTH;
let AI = false;
let score = [-10, 10];
let sum = 0;
let getScore = document.getElementById("score1");
function AIcheck() {
    AI = true
}

function Loaded() {

    mode = 0;
    if (size == 3)
        MAXDEPTH = size * size;
    else if (size <= 6)
        MAXDEPTH = 4;
    else
        MAXDEPTH = 3;
    InGame = true;
    CPlayer = 0;
    l_played = [], l_win = [];
    newBoard();
}

function Click(id) {
    if (!InGame)
        return;
    let tmp= document.getElementById("score1").textContent;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    if (square.item(pos).getAttribute("player") != "-1")
        return;
    let path = "url('../../../static/images/player_2.png')";
    if (CPlayer == 1) {
        path = "url('../..../static/images/player_1.png')";
    }
    square.item(pos).style.backgroundImage = path;
    square.item(pos).setAttribute("player", CPlayer.toString());
    l_played.push(pos);
    let win = WinGame();
    let pwin = CPlayer;
    if (!AI) {
        if (CPlayer == 0) {
            CPlayer = 1;
        } else {
            CPlayer = 0;
        }
    } else {
        if (!win) {
            AIMode();
            win = WinGame();
            pwin = 1;
        }
    }
    if (win) {
        let mess;
        if (pwin == 0) {
            mess = 'Player with "O" win';
            sum = 10;
            getScore.textContent = parseInt(tmp)+sum;
        } else if (pwin == 1) {
            mess = 'Player with "X" win';
            sum = 0;
            getScore.textContent =parseInt(tmp)+ sum;
        }

        alert(mess);
        InGame = false;
        let square = document.getElementsByClassName("square");
        for (x = 0; x < size; x++) {
            for (y = 0; y < size; y++) {
                let pos = parseInt(x + y * size);
                square.item(pos).style.backgroundColor = "#FFF";
            }
        }
        win = WinGame();
    } else if (l_played.length == size * size && !win) {
        alert("Draw!!");
        InGame = false;
        sum = 5;
        getScore.textContent =parseInt(tmp)+ sum;
        let square = document.getElementsByClassName("square");
        for (x = 0; x < size; x++) {
            for (y = 0; y < size; y++) {
                let pos = parseInt(x + y * size);
                square.item(pos).style.backgroundColor = "#FFF";
            }
        }
        win = WinGame();
    }
}

function MouseOver(id) {
    if (!InGame) return;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    square.item(pos).style.backgroundColor = "#3F3";
}

function MouseOut(id) {
    if (!InGame) return;
    let square = document.getElementsByClassName("square");
    let pos = parseInt(id);
    square.item(pos).style.backgroundColor = "#FFF";
}

function minab(a, b) {
    if (a < b) return a;
    else return b;
}

function maxab(a, b) {
    if (a > b) return a;
    else return b;
}

function WinGame() {
    let result = false;
    let Board = GetBoard();
    for (x = 0; x < size; x++) {
        for (y = 0; y < size; y++) {
            if (winHor(x, y, Board) || winVer(x, y, Board) || winCross1(x, y, Board) || winCross2(x, y, Board)) {
                let square = document.getElementsByClassName("square");
                for (i = 0; i < l_win.length; i++) {
                    square.item(l_win[i]).style.backgroundColor = "#F3EC20";
                }
                result = true;
            }
        }
    }
    return result;
}

function winHor(x, y, Board) {
    l_win = [];
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1)
        return false;
    if (x > 0) {
        let p = Board[x - 1 + y * size];
        if (p != player && p != -1)
            counto++;
    }
    for (i = x; i < size; i++) {
        let p = Board[i + y * size];
        if (p == player && p != -1) {
            count++;
            l_win.push(i + y * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winVer(x, y, Board) {
    l_win = [];
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1) return false;
    if (y > 0) {
        let p = Board[x + (y - 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = y; i < size; i++) {
        let p = Board[x + i * size];
        if (p == player && p != -1) {
            count++;
            l_win.push(x + i * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winCross1(x, y, Board) {
    l_win = [];
    if (x > size - countmax || y < countmax - 1) return false;
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1)
        return false;
    if (y < size - 1 && x > 0) {
        let p = Board[x - 1 + (y + 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = 0; i <= minab(size - x, y); i++) {
        let p = Board[(x + i) + (y - i) * size];
        if (p == player && p != -1) {
            count++;
            l_win.push((x + i) + (y - i) * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function winCross2(x, y, Board) {
    l_win = [];
    if (x > size - countmax || y > size - countmax) return false;
    let count = 0,
        counto = 0;
    let player = Board[x + y * size];
    if (player == -1) return false;
    if (y > 0 && x > 0) {
        let p = Board[x - 1 + (y - 1) * size];
        if (p != player && p != -1) counto++;
    }
    for (i = 0; i < minab(size - x, size - y); i++) {
        let p = Board[(x + i) + (y + i) * size];
        if (p == player && p != -1) {
            count++;
            l_win.push((x + i) + (y + i) * size);
        } else { if (p != -1) counto++; break; };
    }
    if (count >= countmax) {
        if (mode == 0)
            return true;
        else {
            if (counto >= 2) return false;
            else return true;
        }
    }
    return false;
}

function GetBoard() {
    let TBoard = [];
    let sqr = document.getElementsByClassName("square");
    for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
            TBoard.push(parseInt(sqr.item(x + y * size).getAttribute("player")));
        }
    }
    return TBoard;
}

function Reset() {
    AI = false;
    InGame = true;

}

function newBoard() {
    let table = document.getElementById("table");
    let row = document.getElementsByClassName("row");
    let square = document.getElementsByClassName("square");
    // Create Table
    table.innerHTML = "";
    for (y = 0; y < size; y++) {
        table.innerHTML += '<tr class="row"></tr>';
        for (x = 0; x < size; x++) {
            let div = '<div class="square" onClick="Click(id)" onMouseOver="MouseOver(id)" onMouseOut="MouseOut(id)"></div>';
            row.item(y).innerHTML += '<td class="col">' + div + '</td>';
            square.item(x + y * size).setAttribute("id", (x + y * size).toString());
            square.item(x + y * size).setAttribute("player", "-1");
        }
    }
}

function PvsM() {
    AI = true;
    InGame = true;
    Loaded();
}

function AIMode() {
    if (!InGame) return;
    let vmax = -Infinity;
    let px = -1;
    let py = -1;
    let TBoard = GetBoard();
    for (let y = 0; y < size; y++) {
        for (let x = 0; x < size; x++) {
            if (TBoard[x + y * size] == -1) {
                TBoard[x + y * size] = 1;
                let mark = minimax(TBoard, 0, false, -Infinity, Infinity);
                TBoard[x + y * size] = -1;
                if (mark > vmax) {
                    px = x;
                    py = y;
                    vmax = mark;
                }
            }
        }
    }
    try {
        let sqr = document.getElementsByClassName("square");
        sqr.item(px + py * size).setAttribute("player", "1");
        sqr.item(px + py * size).style.backgroundImage = "url('../../../static/images/player_1.png')";
        l_played.push(px + py * size);
    } catch (e) {}
}

function GetMark(TBoard) {
    let result = null;
    let openSpots = 0;
    for (let x = 0; x < size; x++) {
        for (let y = 0; y < size; y++) {
            if (winHor(x, y, TBoard) || winVer(x, y, TBoard) || winCross1(x, y, TBoard) || winCross2(x, y, TBoard)) {
                let player = TBoard[x + y * size];
                if (player == 1)
                    result = 1;
                else if (player == 0)
                    result = 0;
            }
            if (TBoard[x + y * size] == -1) {
                openSpots++;
            }

        }
    }

    if (result == null && openSpots == 0) {
        return -1;
    } else
        return result;
}

function minimax(TBoard, depth, isMaximizing, alpha, beta) {
    let result = GetMark(TBoard);
    if (result != null) {

        if (result == 1)
            return score[1] + depth;
        else if (result == 0)
            return score[0] + depth;
        else return 0;
    }
    depth = depth + 1;
    if (depth > MAXDEPTH)
        return 0;
    if (isMaximizing) {
        let bestScore = -Infinity;
        for (let y = 0; y < size; y++) {
            for (let x = 0; x < size; x++) {
                if (TBoard[x + y * size] == -1) {
                    TBoard[x + y * size] = 1;
                    let score = minimax(TBoard, depth, false, alpha, beta);
                    TBoard[x + y * size] = -1;
                    bestScore = maxab(score, bestScore);
                    alpha = maxab(alpha, bestScore);
                    if (beta <= alpha)
                        return;

                }
            }
        }
        return bestScore;
    } else {
        let bestScore = Infinity;
        for (let y = 0; y < size; y++) {
            for (let x = 0; x < size; x++) {
                if (TBoard[x + y * size] == -1) {
                    TBoard[x + y * size] = 0;
                    let score = minimax(TBoard, depth, true, alpha, beta);
                    TBoard[x + y * size] = -1;
                    bestScore = minab(score, bestScore);
                    beta = minab(beta, bestScore);
                    if (beta <= alpha)
                        return;
                }
            }
        }
        return bestScore;
    }
}